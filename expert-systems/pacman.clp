(deftemplate figure
(slot x (allowed-numbers 0 1 2 3 4 5 6 7))
(slot y (allowed-numbers 0 1 2 3 4 5 6 7))
(slot type (allowed-values pac-man monster food))
(slot direction (allowed-values up down left right none) (default none))
)
(deftemplate wall
(slot x1 (allowed-numbers 0 1 2 3 4 5 6 7))
(slot x2 (allowed-numbers 0 1 2 3 4 5 6 7))
(slot y1 (allowed-numbers 0 1 2 3 4 5 6 7))
(slot y2 (allowed-numbers 0 1 2 3 4 5 6 7))
)

(defrule finish-game
(not (figure (type food)))
?figure <-(figure (type ~food) (direction ~none))
=>
(printout t "Pacman wins " crlf)
(facts)
(reset)
)
(defrule pac-man-eats-food
?food <- (figure (x ?x) (y ?y) (type food))
(figure (x ?x) (y ?y) (type pac-man))
=>
(retract ?food)
(printout t "Pac-man eats food "  ?x "," ?y  crlf)
)
(defrule monster-kills-pac-man
?pac-man <- (figure (x ?x) (y ?y) (type pac-man))
(figure (x ?x) (y ?y) (type monster))
=>
(retract ?pac-man)
(printout t "Monster killed Pac-man "  ?x "," ?y  crlf)
(printout t "Pac-man loses game "   crlf)

(facts)
(reset)
)


(defrule figure-moves-to-up
(wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
?figure <-(figure (x ?x) (y ?y) (type ?t) (direction up))
(test (neq ?t food))
(test (> ?y 0))
(test (eq ?y1 ?y2))
(test (or (neq ?y1 :(- ?y 1)) (> ?x1 ?x) (< ?x2 ?x)))
=>
(modify ?figure (y (- ?y 1)))
(printout t "Up " ?t ": " ?x "," ?y " => " ?x "," (- ?y 1)  crlf)
)


(defrule figure-moves-to-down
(wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
?figure <-(figure (x ?x) (y ?y) (type ?t) (direction down))
(test (neq ?t food))
(test (< ?y 7))
(test (eq ?y1 ?y2))
(test (or (neq ?y1 :(+ ?y 1)) (> ?x1 ?x) (< ?x2 ?x)))

=>
(modify ?figure (y (+ ?y 1)))
(printout t "Down " ?t ": " ?x "," ?y " => " ?x "," (+ ?y 1)  crlf)
)
(defrule figure-moves-to-left
(wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
?figure <-(figure (x ?x) (y ?y) (type ?t) (direction left))
(test (> ?x 0))
(test (neq ?t food))
(test (eq ?x1 ?x2))
(test (or (neq ?x1 :(- ?x 1)) (> ?y1 ?y) (< ?y2 ?y)))
=>
(modify ?figure (x (- ?x 1)))
(printout t "left " ?t ": " ?x "," ?y " => " (- ?x 1) "," ?y  crlf)
)
(defrule figure-moves-to-right
(wall (x1 ?x1) (x2 ?x2) (y1 ?y1) (y2 ?y2))
?figure <-(figure (x ?x) (y ?y) (type ?t) (direction right))
(test (< ?x 7))
(test (neq ?t food))
(test (eq ?x1 ?x2))
(test (or (neq ?x1 :(+ ?x 1)) (> ?y1 ?y) (< ?y2 ?y)))
=>
(modify ?figure (x (+ ?x 1)))
(printout t "right " ?t ": " ?x "," ?y " => " (+ ?x 1) "," ?y   crlf)
)
(defrule change-direction-to-figure
?pacman <- (figure (type ?t) (direction ?direction))
(test (neq ?t food))
=>

  (switch (mod (random 0 100) 4)
  (case 0 then
    (modify ?pacman (direction up))
    (printout t ?t " direction up" crlf)
  )
  (case 1 then
    (modify ?pacman (direction down))
    (printout t ?t " direction down" crlf)
  )
  (case 2 then
    (modify ?pacman (direction left))
    (printout t ?t " direction left" crlf)
  )
  (case 3 then
    (modify ?pacman (direction right))
    (printout t ?t  " direction right" crlf)
  )
  )
)

(defrule pac-man-wins-game
(not (figure (type food)))
(figure (type pac-man) (direction ~none))
=>
(printout t "Pac-man wins game")
(facts)
(reset)
)
(defrule pac-man-loses-game
(not (figure (type pac-man)))
(figure (type monster) (direction ~none))
=>
(facts)
(reset)
(printout t "Pac-man loses game")
)

(assert (figure (x 7) (y 7) (type monster) (direction up)))
(assert (figure (x 2) (y 6) (type pac-man) (direction up)))
(assert (wall (x1 2) (x2 6) (y1 3) (y2 3)))
(assert (wall (x1 4) (x2 4) (y1 5) (y2 7)))(assert (figure (x 1) (y 1) (type food)))
;(assert (figure (x 2) (y 2) (type food)))
;(assert (figure (x 3) (y 3) (type food)))
(assert (figure (x 4) (y 4) (type food)))
(assert (figure (x 0) (y 0) (type food)))
(assert (figure (x 0) (y 1) (type food)))
(assert (figure (x 0) (y 2) (type food)))
(assert (figure (x 0) (y 3) (type food)))
(assert (figure (x 0) (y 4) (type food)))
(assert (figure (x 0) (y 5) (type food)))
(assert (figure (x 0) (y 6) (type food)))
(assert (figure (x 0) (y 7) (type food)))
(assert (figure (x 0) (y 0) (type food)))
(assert (figure (x 1) (y 0) (type food)))
(assert (figure (x 2) (y 0) (type food)))
(assert (figure (x 3) (y 0) (type food)))
(assert (figure (x 4) (y 0) (type food)))
(assert (figure (x 5) (y 0) (type food)))
(assert (figure (x 6) (y 0) (type food)))
(assert (figure (x 7) (y 0) (type food)))
;(assert (figure (x 5) (y 5) (type food)))
(assert (figure (x 6) (y 6) (type food)))
(assert (figure (x 6) (y 4) (type food)))

    ;    0  1  2  3  4  5  6  7
    ; 0  |  |  |  |  |  |  |  |
    ; 1  |        |           |
    ; 2  |  |  |  |     |     |
    ; 3  |  |  |  |  |  |     |
    ; 4  |        |  .  |  .  |
    ; 5  |  *  |  |  |  |  #  |
    ; 6  |              |  .  |
    ; 7  |  |  |  |  |  |  |  |
