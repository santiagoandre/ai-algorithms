#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import array
import random
import json

import numpy

from deap import algorithms
from deap import base
from deap import creator
from deap import tools

# gr*.json contains the distance map in list of list style in JSON format
# Optimal solutions are : gr17 = 2085, gr24 = 1272, gr120 = 6942
with open("OchoReinas.json", "r") as tsp_data:
    tsp = json.load(tsp_data)

IND_SIZE = tsp["TourSize"]
MAX_FISTNESS = tsp["TourSize"]

creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", array.array, typecode='i', fitness=creator.FitnessMin)

toolbox = base.Toolbox()

# Attribute generator

#el inidividuo esta mal
#individuo:
#   i = [rowReinaCol0,rowReina1, ... rowReinai,  ... rowReinan-1]
#       donde  n es  8
toolbox.register("indices", random.sample, range(IND_SIZE), IND_SIZE)

# Structure initializers
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

def evalCruzes(x):
    width = len(x)
    fitness = 0
    for row,col in enumerate(x) :

        #if pos not in diagonals.keys() and  not [1 for pos_diag in diagonals.values() if pos in pos_diag]:
        #si no esta en una dagonal derecha
        for difrow,coli in enumerate(x[row+1:]):
            difrow  += 1 # para qe empieze en 1, ya que empizea con la reina de la fila siguiente
            difcol  = coli - col

            if difrow == abs(difcol):
                #print("    ",difrow,difcol)
                fitness += 1
    return (fitness,)
toolbox.register("mate", tools.cxPartialyMatched)


toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.15)
toolbox.register("select", tools.selTournament, tournsize=5)
toolbox.register("evaluate", evalCruzes)

def main():
    random.seed(80)

    pop = toolbox.population(n=100)

    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    algorithms.eaSimple(pop, toolbox, 0.7, 0.2, 50, stats=stats,
                        halloffame=hof)
    print(hof)

    return pop, stats, hof

if __name__ == "__main__":
    main()
    #ind = [6,3,2,7,1,4,0,5]
    #print(evalCruzes(ind))
