#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import array
import random
import json

import numpy

from deap import algorithms
from deap import base
from deap import creator
from deap import tools

# gr*.json contains the distance map in list of list style in JSON format
# Optimal solutions are : gr17 = 2085, gr24 = 1272, gr120 = 6942
with open("cortadora.json", "r") as tsp_data:
#with open("flow-shop1.json", "r") as tsp_data:
#with open("flow-shop.json", "r") as tsp_data:
    tsp = json.load(tsp_data)

Elements = tsp["Elements"]
WidthLamina = tsp["Width"]
IND_SIZE = len(Elements)
creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", array.array, typecode='i', fitness=creator.FitnessMin)

toolbox = base.Toolbox()

# Attribute generator
toolbox.register("indices", random.sample, range(IND_SIZE), IND_SIZE)


# Structure initializers
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

def evalTSP(individual):
    width = 0
    height = 0
    maxheight = 0
    tira = 2
    for pos_element in individual:
        element =  Elements[pos_element]
        width += element[0]
        if width > WidthLamina:# no cabe en esa tira,
            height+=maxheight
            maxheight = element[1]*element[2]
            width = element[0]
            tira+=1
        elif maxheight < element[1]*element[2]:
            maxheight = element[1]*element[2]
    height+=maxheight
    return height,



def interpretar(individual):
    width = 0
    height = 0
    maxheight = 0
    tira = 2
    print("Tira: 1 ")
    for pos_element in individual:
        element =  Elements[pos_element]
        width += element[0]
        if width > WidthLamina:# no cabe en esa tira,

            print("largo tira "+str(tira-1)+" = "+str(maxheight))
            print("Tira: "+str(tira))
            height+=maxheight
            maxheight = element[1]*element[2]
            width = element[0]
            tira+=1
        elif maxheight < element[1]*element[2]:
            maxheight = element[1]*element[2]
        print("         "+ str(Elements[pos_element]))
    if maxheight > 0 :
        print("largo tira "+str(tira-1)+" = "+str(maxheight))
        height+=maxheight
    print("Largo nesesario de la lamina: "+str(height))





toolbox.register("mate", tools.cxPartialyMatched)


toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.15)
toolbox.register("select", tools.selTournament, tournsize=5)
toolbox.register("evaluate", evalTSP)

def main():
    random.seed(80)

    pop = toolbox.population(n=100)

    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    algorithms.eaSimple(pop, toolbox, 0.7, 0.2, 50, stats=stats,
                        halloffame=hof)
    print(interpretar(hof[0]))

    return pop, stats, hof

if __name__ == "__main__":
    main()
    #ind = [1,0,4,3,2]
    #print(evalTSP(ind))
