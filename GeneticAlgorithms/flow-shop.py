#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import array
import random
import json

import numpy

from deap import algorithms
from deap import base
from deap import creator
from deap import tools

# gr*.json contains the distance map in list of list style in JSON format
# Optimal solutions are : gr17 = 2085, gr24 = 1272, gr120 = 6942
with open("flow-shop2.json", "r") as tsp_data:
#with open("flow-shop1.json", "r") as tsp_data:
#with open("flow-shop.json", "r") as tsp_data:
    tsp = json.load(tsp_data)

times_map = tsp["TimesMatrix"]
IND_SIZE = tsp["TourSize"]

creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", array.array, typecode='i', fitness=creator.FitnessMin)

toolbox = base.Toolbox()

# Attribute generator
toolbox.register("indices", random.sample, range(IND_SIZE), IND_SIZE)

# Structure initializers
toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

def evalTSP(individual):
    #print("individual: ",individual)
    up_times,left_times = [],[]
    i = 0
    for task in (individual):
        up_time = times_map[0][task]
        if i == 0:
            up_times.append(up_time)
        else:
            up_times.append(up_times[-1]+up_time)
        i+=1
    i = 0
    #print(individual[1])
    for time in times_map:
        left_time = time[individual[0]]
        if i == 0:
            left_times.append(left_time)
        else:
            left_times.append(left_times[-1]+left_time)
        i+=1
    #print(up_times,left_times)
    left_times = left_times[1:]
    i,j = 1,1
    for up_time in up_times[1:]:
        j = 1
        #print("i: ",i)
        new_left = []
        print(left_times)
        for left_time in left_times:
            #print("   j: ",j)
            new_time = max(left_time,up_time)+times_map[j][ individual[i] ]
            new_left.append(new_time)
            up_time = new_time #ahora el de arriba es el que acabe de crear
            j+=1
        i +=1
        left_times = new_left


    print(left_times)
    return left_times[-1],





toolbox.register("mate", tools.cxPartialyMatched)


toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.15)
toolbox.register("select", tools.selTournament, tournsize=5)
toolbox.register("evaluate", evalTSP)

def main():
    random.seed(80)

    pop = toolbox.population(n=100)

    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    algorithms.eaSimple(pop, toolbox, 0.7, 0.2, 50, stats=stats,
                        halloffame=hof)
    print(hof)

    return pop, stats, hof

if __name__ == "__main__":
    main()
    #ind = [1,0,4,3,2]
    #print(evalTSP(ind))
