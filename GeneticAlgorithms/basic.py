#    This file is part of DEAP.
#
#    DEAP is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as
#    published by the Free Software Foundation, either version 3 of
#    the License, or (at your option) any later version.
#
#    DEAP is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU Lesser General Public License for more details.
#
#    You should h   ave received a copy of the GNU Lesser General Public
#    License along with DEAP. If not, see <http://www.gnu.org/licenses/>.

import array
import random
import math

import numpy as np

from deap import algorithms
from deap import base
from deap import creator
from deap import tools


def configureAlgorithm(toolbox,individual_size,avaluation_f):
    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", array.array, typecode='b', fitness=creator.FitnessMax)
    # Attribute generator
    toolbox.register("attr_bool", random.randint, 0, 1)
    # Structure initializers
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_bool, individual_size)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)

    toolbox.register("evaluate", avaluation_f)
    toolbox.register("mate", tools.cxOnePoint)
    toolbox.register("mutate", tools.mutFlipBit, indpb=0.1)
    toolbox.register("select", tools.selTournament, tournsize=3)


def evalOneMax(x):
    a= 20
    b = 0.2
    c = 2*math.pi
    d = len(x)
    sumx2= sum([(xi*xi) for xi in x])
    sumcosx= sum([math.cos(c*xi) for xi in x])
    return(-a*math.exp(-b*math.sqrt(sumx2/d)) -math.exp(sumcosx/d) +a+math.exp(1),)

def main():
    whole_part = 3
    decimal_part = 5
    genome_size =   1  +  whole_part  +   decimal_part
    ngenomes = 8
    def to_value(individual):
        all_value = []
        for i in range(ngenomes):
            limit = i+genome_size
            sign = individual[i]
            n = whole_part-1
            value = 0
            for bit in individual[i+1:limit]:
                value += bit*math.pow(2,n)
                n-=1
            if  not sign:
                value = -value
            all_value.append(value)
        return all_value
    toolbox = base.Toolbox()
    configureAlgorithm(toolbox,genome_size*ngenomes,lambda x: evalOneMax(to_value(x)))
    random.seed(20)

    pop = toolbox.population(n=20)
    print(pop)
    hof = tools.HallOfFame(4)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)
    pop, log = algorithms.eaSimple(pop, toolbox, cxpb=0.5, mutpb=0.2, ngen=80,
                                   stats=stats, halloffame=hof, verbose=True)
    for hofi in hof:
        value =  to_value(hofi)
        print("hof: ",hofi," = ",value, "fintess: ",evalOneMax(value)[0])
        pass
    return pop, log, hof

if __name__ == "__main__":
    main()
