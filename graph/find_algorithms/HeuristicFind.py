# -*- coding: utf-8 -*-


from collections import deque


class HeuristicFind():

    def __init__(self,problem):
        self.problem=problem


    def run(self,maxDepth=6):
        open_list= []
        close_list=deque()
        starting_node=self.problem.Initial_Node
        open_list.append(starting_node)
        solution=[]
        iterations = 1
        while len(open_list) > 0:
            current_node=open_list.pop()
            #print('Current_Node:'+str(current_node.State))
            close_list.append(current_node)
            if self.problem.goal_test(current_node):
                print("Iterations: ",iterations)
                return  current_node.Depth,self.problem.solution(current_node)
            if current_node.Depth >= maxDepth:
                continue
            children= self.problem.child_node(current_node)
            open_list = self.add_chlidrens(open_list,children)
            open_list = sorted(open_list,key = (lambda node: node.path_cost),reverse = True)#aqui se ordena la lista de acurdo a la heuristica
            iterations+=1
        print("Iterations: ",iterations)
    def add_chlidrens(self,open_list,children):
        new_list = []
        for child in children:
            self.problem.calculate_heuristic(child)
        #if not open_list:
        for node in open_list:
            append = False
            for child in children:
                if node == child:
                    if (node.path_cost)>  (child.path_cost):
                        new_list.append(child)
                        print("Set for new optimal node")
                    else:
                        new_list.append(node)
                    append = True
                    children.remove(child)
                    continue
            if not append:
                new_list.append(node)
        for child in children:
            new_list.append(child)
        return new_list

                #print('Child:',child.State,'Depth: ', child.Depth)
    def quicksort(self,list):
        if not list:
            return []
        list = list(list)
        x = list[0]
        #print("list: ",list)
        xs = list[1:]
        smallerSorted = self.quicksort( [a for a in xs if a<=x])
        biggerSorted  = self.quicksort( [a for a in xs if a>x])
        return  smallerSorted + [x] + biggerSorted
