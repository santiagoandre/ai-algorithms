# -*- coding: utf-8 -*-


from collections import deque


class BreadthFirst():

    def __init__(self,problem):
        self.problem=problem


    def run(self):
        open_list=deque()
        close_list=deque()
        starting_node=self.problem.Initial_Node
        open_list.append(starting_node)
        solution=[]
        iterations = 1
        while len(open_list) > 0:
            current_node=open_list.popleft()

            #print('Current_Node:'+str(current_node.State))

            close_list.append(current_node)

            if self.problem.goal_test(current_node):
                print("Iterations: ",iterations)
                return current_node.Depth,self.problem.solution(current_node)
            children= self.problem.child_node(current_node)

            for child in children :
                if child not in open_list and child not in close_list:
                    #print('Child:'+str(child.State))
                    open_list.append(child)
            iterations += 1
