# -*- coding: utf-8 -*-


from collections import deque
from find_algorithms.DepthFirst import DepthFirst

class DepthFirstDecorated(DepthFirst):

    def run(self,initDepth,maxDepth = 30, increment = 5):
        depth = initDepth
        while depth<= maxDepth:
            print("test in depth ",depth)
            solution = DepthFirst.run(self,depth)
            if solution is not None:
                return solution
            depth = depth + increment
