# -*- coding: utf-8 -*-


from collections import deque


class DepthFirst:

    def __init__(self,problem):
        self.problem=problem


    def run(self,maxDepth = 5):
        open_list=[]
        close_list=deque()
        starting_node=self.problem.Initial_Node
        open_list.append(starting_node)
        solution=[]
        iterations = 1
        while len(open_list) > 0:
            current_node=open_list.pop()
            print('Current_Node:'+str(current_node.State))
            close_list.append(current_node)
            if self.problem.goal_test(current_node):
                print("Iterations: ",iterations)
                return  current_node.Depth,self.problem.solution(current_node)
            if current_node.Depth >= maxDepth:
                continue
            children= self.problem.child_node(current_node)
            for child in children :
                if child not in open_list and child not in close_list:
                    open_list.append(child)
                    print('Child:',child.State,'Depth: ', child.Depth)
            iterations += 1
