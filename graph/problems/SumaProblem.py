# -*- coding: utf-8 -*-

from Node import Node
from .Problem import Problem

class SumaProblem(Problem):

    def __init__(self,elements_list,sumaSolucion,initial_state=[]):
        operations = {0:self.add}
        self.elements_list = elements_list
        self.sumaSolucion = sumaSolucion
        #    def __init__(self,initial_state,final_state = None ,operations):
        super().__init__(initial_state,None,operations)

    def child_node(self,node):
        children = []
        for pos in range(len(self.elements_list)):
            son=self.add(pos,node)
            if(son!=None):
                children.append(son)
        return children
    def add(self,pos,node):
        state=node.State
        if pos in state:
            return None
        new_state = list(state)
        new_state.append(pos)
        sum = self.sum(new_state)
        if sum<=self.sumaSolucion:
            return Node(new_state,parent=node,action='add')

    def sum(self,state):
        sum =0
        for pos in state:
            sum = sum + self.elements_list[pos]
        return sum

    def goal_test(self,node):
        sum = self.sum(node.State)
        return sum==self.sumaSolucion
    def solution(self,current_node):
        solution = []
        for pos in current_node.State:
            solution.append(self.elements_list[pos])
        return solution
