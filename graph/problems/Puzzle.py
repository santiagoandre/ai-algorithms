# -*- coding: utf-8 -*-

from Node import Node
from .Problem import Problem

class Puzzle(Problem):

    def __init__(self,initial_state,final_state,size = (3,3)):
        operations = {0:self.up,
                      1:self.down,
                      2:self.left,
                      3:self.right
        }
        self.size = size
        super().__init__(initial_state,final_state,operations)

    def child_node(self,*args):
        [node] = args
        vpos = Puzzle.pos(node, self.size)
        return Problem.child_node1(self,vpos,node)

    def up(self,pos,node):
        (x,y) = pos
        state=node.State
        width, height = self.size
        if y == (height-1):
            return None
        new_state = Puzzle.move(node.State,Puzzle.to_linear_pos(x,y+1, self.size),Puzzle.to_linear_pos(x,y, self.size))
        new_node=Node(new_state,parent=node,action='up')
        return new_node
    def down(self,pos,node):
        (x,y) = pos
        state=node.State
        width, height =  self.size
        if y == 0:
            return None
        new_state = Puzzle.move(node.State,Puzzle.to_linear_pos(x,y-1, self.size),Puzzle.to_linear_pos(x,y, self.size))
        new_node=Node(new_state,parent=node,action='down')
        return new_node
    def left(self,pos,node):
        (x,y) = pos
        state=node.State
        width, height = self.size
        if x == (width-1):
            return None
        new_state = Puzzle.move(node.State,Puzzle.to_linear_pos(x+1,y, self.size),Puzzle.to_linear_pos(x,y, self.size))
        new_node=Node(new_state,parent=node,action='left')
        return new_node
    def right(self,pos,node):
        (x,y) = pos
        state=node.State
        width, height = self.size
        if x == 0:
            return None
        new_state = Puzzle.move(node.State,Puzzle.to_linear_pos(x-1,y, self.size),Puzzle.to_linear_pos(x,y, self.size))
        new_node=Node(new_state,parent=node,action='right')
        return new_node
    def pos(node,size):
        ''' obtiene la posicion (x,y) de la celda vacia en el tablero'''
        width, height = size
        x,y = 0,0
        for celda in node.state:
            if x == width:
                y = y + 1
                x = 0
            if celda == 0:
                return x,y
            x = x +1
    def move(state,init_linear_pos, final_linear_pos):
        new_state = list(state)
        new_state[final_linear_pos] = new_state[init_linear_pos]
        new_state[init_linear_pos] = 0
        return new_state
    def to_linear_pos(x,y,size):
        width , _ =  size
        '''trasnforma una posicion (x,y) en la posicion del vector que contiene las fichas del tablero'''
        return y*width+x
