# -*- coding: utf-8 -*-

from abc import ABC, abstractmethod
from Node import Node


class Problem(ABC):

    def __init__(self,initial_state,final_state ,operations):
        self.initial_node = Node(initial_state)
        self.final_node = Node(final_state)
        self.operations = operations
        super().__init__()
    def child_node1(self,*args):
        child=[]
        #Aplicar Operaciones sobre el estado actual para generar nuevos estados
        for operation in self.operations.values():
            son=operation(*args)
            if(son!=None):
                child.append(son)
        return child

    @property
    def Operations(self):
        # Do something if you want
        return self.operations

    @abstractmethod
    def child_node(self,node):
        pass


    def goal_test(self,node):

        if isinstance(node, Node):
            if self.final_node == node:
                return True
            else:
                return False
        else:
            return False

    @property
    def Initial_Node(self):
        # Do something if you want
        return self.initial_node

    @Initial_Node.setter
    def Initial_Node(self,node):
        # Do something if you want
        self.initial_node = node

    @property
    def Goal_Node(self):
        # Do something if you want
        return self.goal_node

    @Goal_Node.setter
    def Goal_Node(self,node):
        # Do something if you want
        self.goal_nodee = node
    def solution(self,current_node):
        solution = []
        while current_node.Parent!=None:
            solution.append(current_node.Action)
            current_node=current_node.Parent
        solution.reverse()
        return solution
