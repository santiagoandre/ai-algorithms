# -*- coding: utf-8 -*-

from find_algorithms.BreadthFirst import BreadthFirst
from problems.SumaProblem import SumaProblem
class SumTest:
    def start(self):
        #self,elements_list,sumaSolucion,initial_state=[]):
        p=SumaProblem((1,3,5,7,5,11,13,15),34)


        bf=BreadthFirst(p)

        sol=bf.run()
        print('Solution: '+str(sol))
