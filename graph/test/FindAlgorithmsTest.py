# -*- coding: utf-8 -*-

from find_algorithms.BreadthFirst import BreadthFirst
from problems.WaterJug import WaterJug
from problems.Puzzle import Puzzle
class FindAlgorithmsTest:
    def start(self):
        pz=Puzzle([1,2,3,4, 5,6,7,8 ,9,10,11,0],[1,2,0,4, 5,7,3,8 ,9,6,10,11],(4,3))

        #wj=WaterJug(4,3,(4,3),(2,0))
        '''
        1,2,3,4, 5,6,7,8 ,9,10,11,12
        1 0 2
        8 4 3
        7 6 5
        1 2 3
        8 4 5
        7 6 0
        '''
        #wj=WaterJug()

        '''
        bf=BreadthFirst(pz)
        '''
        bf=BreadthFirst(pz)

        sol=bf.run()
        print('Solution: '+str(sol))
