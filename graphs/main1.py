from collections import deque
from find_algorithms.NearestNeighbors import NearestNeighbors as NeaNe
from problems.StackBoxes import StackBoxes

print("StackBoxes problem")
boxes = ['A','B','C']
init_state = []
final_state = ['C','A','D']
problem = StackBoxes(boxes,init_state,final_state)
a = NeaNe(problem)
a.run()
