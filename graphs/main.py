from resourses.file_graph import *
from logic.graph.graphs import *
def testGraph(graph):
	graph.add_node(1)
	graph.add_node(2)
	graph.add_node(3)
	graph.add_node(4)
	graph.add_node(5)
	graph.del_node(1)
	graph.add_edge((2,2))
	graph.add_edge((2,3))
	graph.add_edge((2,2))
	graph.add_edge((3,4))
	graph.add_edge((3,4))
	print (graph.edges())
	graph.del_edge((3,2))
	graph.del_edge((3,3))
def testWeightedGraph(graph):
	graph.add_node(1)
	graph.add_node(2)
	graph.add_node(3)
	graph.add_node(4)
	graph.add_node(5)
	graph.del_node(1)
	graph.add_edge((2,2,1))
	graph.add_edge((2,3,9))
	graph.add_edge((2,2,2))
	graph.add_edge((3,4,1))
	graph.add_edge((3,4,34))
	print (graph.edges())
	graph.del_edge((3,2,1))
	graph.del_edge((3,3,1))

def load_file():
	global T
	global N
	global E
	global P
	if 'P' not in globals():
		P = True
	else:
		P = P in ["Y","y"]

	if 'T' not in globals():
		T = 1
	if T == 1:
		print("Undirected", end=" ")
		graph = UndirectedGraph(P)
	elif T == 2:
		print("Directed",end=" ")
		graph = DirectedGraph(P)
	else:
		raise Exception("invalid graph type")
	if P:
		print("Weighted Graph")
	else:
		print("Graph")
	if 'N'  in globals():
		repeated_nodes = graph.add_nodes(N)
		if len(repeated_nodes):
			print("Some nodes was repeated: ",repeated_nodes)
	if 'E' in globals():
		repeated_edges = graph.add_edges(E)
		if len(repeated_edges):
			print("some edges were refused: ",repeated_edges)
	return graph
if __name__== "__main__":

	graph = load_file()
	print(graph.nodes())
	print(graph.edges())
