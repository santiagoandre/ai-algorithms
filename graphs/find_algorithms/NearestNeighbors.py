# -*- coding: utf-8 -*-

from collections import deque


class NearestNeighbors():

    def __init__(self,problem):
        self.problem=problem


    def generate_tree(self):

        open_list=deque()
        close_list=deque()
        starting_node=self.problem.graph.add_node([])
        open_list.append(starting_node)
        solution=[]

        while len(open_list) > 0:
            current_node=open_list.popleft()
            #print('Current_Node:'+str(current_node.State))
            close_list.append(current_node)
            children= self.problem.child_node(current_node)
            for child in children :
                if child not in open_list and child not in close_list:
                    #print('Child:'+str(child.State))
                    open_list.append(child)
        nodes = self.problem.graph.nodes()
        edges= self.problem.graph.edges()
        print(len(nodes)," Nodes: -------")
        for node in nodes:
            print(node)
        print(len(edges)," Edges ------")
        for edge in edges:
            print(edge)
    def find_solution(self):
        graph = self.problem.graph
        Nodes = graph.nodes()
        self.problem.final_node.tag= 0
        print(self.problem.Initial_Node.adjacencies())
        self.tag_nodes(self.problem.final_node.adjacencies(),1)
        print("Solution: ")
        print(self.solution(self.problem.Initial_Node))

    def tag_nodes(self,adjacencies,tag):
        #print("adjacencies: ",adjacencies)
        for (node,_) in adjacencies:
            if node.tag is  None:
                node.tag = tag
                #print(node," tag: ",node.tag)
                if node.__eq__(self.problem.Initial_Node):
                    return
                self.tag_nodes(node.adjacencies(),tag+1)
    def solution(self,initial_node):
        for (node,action) in initial_node.adjacencies():
            if node.tag is None:
                continue
            if self.problem.final_node == node:
                return [action]

            if node.tag < initial_node.tag:
                sol=[action] + self.solution(node)
                #print("in sol",sol)
                return sol

    def run(self):
        self.generate_tree()
        self.find_solution()
    def quicksort(self,list,compareE):
        if not len(list):
            return []
        x = list[0]
        xs = list[1:]

        smallerSorted = self.quicksort( [a for a in xs if compareE(a,x) != 1],compareE)
        biggerSorted  = self.quicksort( [a for a in xs if compareE(a,x) == 1],compareE)
        return  smallerSorted + [x] + biggerSorted
