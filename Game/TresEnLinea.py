from Game import Game
from AlphaBeta import AlphaBeta
class TresEnLinea(Game):
    X = 1
    Y = -1
    Empty = 0
    def display(self,state):
        for i,cell in enumerate(state['board']):
            if i % 3 == 0:
                print()
            if cell == TresEnLinea.X:
                print("X",end = " ")
            elif cell == TresEnLinea.Y:
                print("Y",end=" ")
            else:
                print("_",end= " ")
        print()

    def actions(self, state):
        #print("board:: ",state)
        return [pos for pos,cell in enumerate(state['board']) if cell == TresEnLinea.Empty]
    def result(self, state, pos,create_copy = True):
        #print("pos: ",pos)

        if create_copy:
            new_state = {}
            new_state['board'] = list(state['board'])
            new_state['moves'] = list(state['moves'])
            new_state['to_move'] = state['to_move']
            new_state['utility'] = 0
        else:
            new_state = state
        new_state['board'][pos] = state['to_move']
        new_state['to_move'] = new_state['to_move']*-1 # X = - Y #estoy dandole el turno al siguiente jugador

        #print("moves: ",state['moves'])
        new_state['moves'].remove(pos)
        return  new_state

    def utility(self, state, player):
        winsX,winsY = 0,0
        board = state['board']
        #analizar verticales
        for i in range(3):
            #puede ganar X o Y en esa vertival
            countX,countY = self.count_xy(board,i,i+6,3)
            if countX == 3:
                return float("inf")*player
            elif countY == 3:
                return -float("inf")*player
            else:
                winsX += 1 if countY ==0 else 0
                winsY += 1 if countX ==0 else 0
        #analizar horizontales
        for i in range(0,9,3):
            #puede ganar X o Y en esa horizontal
            countX,countY = self.count_xy(board,i,i+2,1)
            if countX == 3:
                return float("inf")*player
            elif countY == 3:
                return -float("inf")*player
            else:
                winsX += 1 if countY ==0 else 0
                winsY += 1 if countX ==0 else 0
        #analizar vertixcal 1
        countX,countY = self.count_xy(board,0,8,4)
        if countX == 3:
            return float("inf")*player
        elif countY == 3:
            return -float("inf")*player
        else:
            winsX += 1 if countY ==0 else 0
            winsY += 1 if countX ==0 else 0
        #analizar vertixcal 2
        countX,countY = self.count_xy(board,2,6,2)
        if countX == 3:
            return float("inf")*player
        elif countY == 3:
            return -float("inf")*player
        else:
            winsX += 1 if countY ==0 else 0
            winsY += 1 if countX ==0 else 0
        '''
        if player == TresEnLinea.X:
            print("player X")
        else:
            print("player Y")
        print("heuristic: ",(winsX-winsY)*player)

        self.display(state)
        '''
        return  (winsX-winsY)*player# es positiva si el jugador actual tiene mas psivilidades de gamar

    def count_xy(self,board,initial_pos,final_pos,increment):
        countX = 0
        countY = 0
        for i in  range(initial_pos,final_pos+1,increment):
            cell =  board[i]
            if cell ==TresEnLinea.Empty:
                continue
            if cell ==TresEnLinea.X:
                countX+=1
            elif cell ==TresEnLinea.Y:
                countY+=1
        return  countX,countY


    def terminal_test(self, state):
        if not state['moves']:
            return True
        winsX,winsY = 0,0
        board =state['board']
        #analizar verticales
        for i in range(3):
            #puede ganar X o Y en esa vertival
            countX,countY = self.count_xy(board,i,i+6,3)
            if countX == 3 or countY == 3:
                return True
        #analizar horizontales
        for i in range(0,7,3):
            #puede ganar X o Y en esa horizontal
            countX,countY = self.count_xy(board,i,i+2,1)
            if countX == 3 or countY == 3:
                return True
        #analizar vertixcal 1
        countX,countY = self.count_xy(board,0,8,4)
        if countX == 3 or countY == 3:
            return True
        #analizar vertixcal 2
        countX,countY = self.count_xy(board,2,6,2)
        if countX == 3 or countY == 3:
            return True
        return False

    def escoger_simbolo(self):
        while(1):
            print("Que simbolo escoges: ")
            print("   - X")
            print("   - Y")
            jugador = input()
            if jugador in ['X','Y']:
                break
            else:
                print("opcion invalida")
        if jugador == "X":
            return TresEnLinea.X
        else:
            return TresEnLinea.Y
    def test_game(self,state):
        if self.terminal_test(state):
            if self.utility(state,TresEnLinea.X):
                print("Win X")
            elif self.utility(state,TresEnLinea.Y):
                print("Win Y")
            else:
                print("tie...")
            return True
    def play_usuario(self,state):
        while 1:
            print("Digite la posicion donde va a atacar: ")
            try:
                posx,posy = eval(input())
                if posx < 0 or posx <0 or posy > 2 or posy > 2:
                    print("posicion invalida")
                    continue
                action = posy*3+posx
                if action not in state['moves']:
                    print("Posicion ocupada.")
                    continue
                break
            except KeyboardInterrupt:
                print("Adios ...")
                exit(0)
            except:
                print("formato invalido: el formato a seguir es: (posx,posy)")
        #action = posy*3+posx
        return action

    def initial_state(self):
        state  = self.state
        state['board']  = [TresEnLinea.Empty for _ in range(9)]
        state['moves'] = self.actions(state)
        #print("iniyial",state)
        return state


    def play_game(self):
        state = self.initial_state()
        cerebro = AlphaBeta(self)
        jugador = self.escoger_simbolo()
        self.display(state)
        print("Quieres empezar?[S/N]")
        if input() in ['S','s']:
            state['to_move'] = jugador
            action = self.play_usuario(state)
            state = self.result(state,action,create_copy = False)
            self.display(state)
        else:
            state['to_move'] = jugador *-1

        #print("Statee: ",state)
        while(1):
            action = cerebro.run(state)
            state = self.result(state,action,create_copy = False)
            self.display(state)
            if self.test_game(state):
                break
            action = self.play_usuario(state)
            state = self.result(state,action,create_copy = False)
            self.display(state)
            if self.test_game(state):
                break
        return state
