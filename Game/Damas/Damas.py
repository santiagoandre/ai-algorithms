from Game import Game
from MVC import Model
from AlphaBeta  import AlphaBeta
import math
from time import sleep
class Damas(Model,Game):

    def __init__(self,width=4):
        self.width = width
        self.moves=[]
        self.simple_chip = SimpleChip(width)
        self.queen_chip = QueenChip(width)
        self.state = None
        self.direction = {True: "U",False: "D"}
        self.views = []
        self.brain = AlphaBeta(self)
        self.weightBoard = self.initWeightBoard()
    def initWeightBoard(self):
        '''inicializar un tablero del mismo tamanio que Board, pero este le da un valor a cada posicion,
           reprecentando en donde estas mejor ubicado:
                - Donde no te pueden atacar.
                - Donde coronas.
        '''
        b,a,incremet =0,(self.width-1), -1
        weightBoard= []
        for _ in range(self.width-1):
            weightBoard+= [i for i in range (a,b,incremet)]+[self.width-2]
            weightBoard+=[self.width-2]+ [i for i in range (b+1,a+1,-incremet)]
        #weightBoard+= [self.width+1 for _ in range(self.width-1)]+[self.width+2]
        weightBoard+= [i for i in range (a,b,incremet)]+[self.width-2]
        weightBoard += [self.width+2 for _ in range(self.width)] #la ultima fila tiene mas peso, porque ahi puede salir una reina, y no te la pueden matar
        print("size widtmap: ",len(weightBoard))
        return weightBoard



    def actions(self, state,player=None):
    #action: [posi,desplazamiento] -> una accion simple
    #stringaction attack: [pos1,pose1,pos2,pose2,.....posf-1,posef-1,posf]
    #pos1 _____
    #     pose1 _____   => la ficha pasa de pos1 a pos2
    #           pos2       matando la ficha que esta en pose1
    #Este metodo retorna una lista de actions y stringactions attacks
        if player is None:
            player = state['to_move']
        board = state['board']
        x,y =0,0
        direction = self.direction[player]
        allSimpleMovements = []
        allAttackMovements = []
        for pos,(player_pos,type) in enumerate(board):
            if player ==player_pos:
                #obtener los movimientos que tiene la ficha
                simpleMovements, attackMovements = type.all_movements(x,y,pos,direction,board)
                if attackMovements:#si a movimientos de ataque, no puede hacer ataques simples
                    #desprecio los movimientos simples
                    #colocar al principio la posicion de la ficha
                    allAttackMovements+= [([pos]+ movement) for movement in attackMovements]
                elif not allAttackMovements:
                    allSimpleMovements+= [(pos,movement) for movement in simpleMovements]

            #obtener cordenadas cartecianas
            x+=1
            if x % self.width == 0:
                y+=1
                x=0
        if allAttackMovements:
            return allAttackMovements
        else:
            return allSimpleMovements




    def result(self, state, action, copy = True):
        #0. Generar una copia del estado
        if copy:
            new_state = {}
            new_state['board'] = list(state['board'])
            new_state['to_move'] = state['to_move']
            new_state['utility'] = 0
        else:
            new_state = state
        #1. obtener la posicion inicial y final de la ficha que se va a mover
        posi,posf = action[0],action[-1]

        board = new_state["board"]
         # 2. es una accion de ataque?, cadena de movimientos=> ver el metodo action
        if isinstance(action,list):
        #2.1 eliminar fichas atacadas, liberar esas posiciones colocando None
            #2.1.1 recorrer accion obteniendo posiciones enemigas
            for i in range(1,len(action),2):#de dos en dos, posiciones enemigas
                pos = action[i]
                #2.1.2 liberar celda donde esta el enemigo
                board[pos] = None,None

        #3. Mover la ficha de la posicion incial a la final
        board[posf] = board[posi] # ocupar posicion
        board[posi] = None,None # liberar posicion
        #4. dar el turno al siguiente jugador
        new_state['to_move'] = not new_state['to_move']
        #5. generar acciones del enemigo en el nuevo estado.
        new_state['moves'] = self.actions(new_state)

        return new_state

    def utility(self, state, player):
        if player:
            player1Actions = sum([(len(action)-1) for action in self.actions(state,not player)])
            player2Actions = sum([(len(action)-1) for action in state['moves']])
        else:
            player2Actions = sum([(len(action)-1) for action in self.actions(state,not player)])
            player1Actions = sum([(len(action)-1) for action in state['moves']])
        nPlayer1Chips, nPlayer2Chips = 0,0
        weight1,weight2 = 0,0
        dispercion1,dispercion2 = (0,float("inf")),(0,float("inf"))#(min,max)
        board = state['board']

        x,y =-1,0
        for pos,(player_pos,type) in enumerate(board):

            x+=1
            if x ==self.width :
                y+=1
                x=0

            if player_pos is None:
                continue
            if  player_pos:
                nPlayer1Chips +=1
                weight1 += self.weightBoard[-pos-1]
                if y<dispercion1[0]:
                    dispercion1[0]  = y
                elif y>dispercion1[1]:
                    dispercion1[1]  = y
            else:
                nPlayer2Chips +=1
                weight2 += self.weightBoard[pos]
                if y<dispercion2[0]:
                    dispercion2[0]  = y
                elif y>dispercion1[1]:
                    dispercion2[1]  = y

        #result = nPlayer1Chips+weight1+(-player2Actions) - ( nPlayer2Chips+weight2+(-player1Actions))
        #result = (player1Actions*nPlayer1Chips)/(1+nPlayer2Chips+player2Actions)+weight1 - ( player2Actions*nPlayer2Chips)/(1+nPlayer1Chips+player1Actions) -weight2
        if player2Actions == 0 or nPlayer2Chips:
            result =  float("inf")
        if player1Actions == 0 or nPlayer1Chips:
            result =  -float("inf")
        else:
            #se calcula la heuristica asi: el jugador que va hacia arriba - la del jugador que va hacia abajo
            result =   (nPlayer1Chips+weight1+player1Actions)- (nPlayer2Chips+weight2+player2Actions)
            #result =    (nPlayer1Chips+weight1)*player1Actions/player2Actions- (nPlayer2Chips+weight2)*player2Actions/player1Actions
            #result =
        if  not player:
            result = - result
        return result

    def terminal_test(self, state):
        player = state["to_move"]
        board = state["board"]
        nchipsEnemy = 0
        x,y = -1,0
        has_actions = False
        for pos,(player_pos,type) in enumerate(board):
            x+=1
            if x == self.width:
                x=0
                y+=1
            if player_pos is None:
                continue
            if player ==  player_pos:
                #determinar si tiene esa ficha tiene movimientos
                if not has_actions:
                    actions = type.all_movements(x,y,pos,self.direction[player],board)
                    if actions[0]  or actions[1]:
                        has_actions = True
                elif nchipsEnemy>0:
                    return False
            else:
                if has_actions:
                    return False
                nchipsEnemy+=1

        return True





    def initState(self):
        y = self.width*2#numero de filas, el doble que las columnas #tiene que ser par anton
        nRowsTeam = int(y*3/8)
        nFreeRows = y-nRowsTeam*2
        print("EL tablero es de",(self.width,y))
        print("El numero de filas vacias es: ",nFreeRows)
        print("El numero de filas para cada equipo  es: ",nRowsTeam)
        nTeamChips, nFreeChips = self.width*3, self.width*2#tiene que sumar width*width/2
        blackChips = [(False,self.simple_chip) for _ in range(nRowsTeam*self.width)]#TODO esto tiene que generalizarse
        whiteChips = [(True,self.simple_chip) for _ in range(nRowsTeam*self.width)]#TODO esto tiene que generalizarse
        freeChips =  [(None,None) for _ in range(nFreeRows*self.width)]#TODO esto tiene que generalizarse
        board = []
        board+= blackChips
        board+= freeChips
        board+= whiteChips

        state = {"board":board,"to_move":True,"utility":0,"moves":[]}
        return state
    def excecute_action(self,action):
        self.state = self.result(self.state, action)
        print("Turno de : ",self.state["to_move"])
        print("Ejecutando accion: ",action)
        self.notify()
        if self.terminal_test(self.state):
            print("Juego terminado")
            return False
        return True

    def play_game(self, player):
        state =  self.initState()
        self.state  =state
        state["to_move"] = player
        state["moves"] = self.actions(state)
        self.notify()
    def play_machine(self):
        print("Play machine")
        action = self.brain.run(self.state,prof=6)
        #sleep(0.2)
        return self.excecute_action(action)
    def play_user(self,action):
        print("Play user")
        actions =self.state['moves']
        print(actions)
        complete_action = None
        for actioni in actions:
            if actioni[0] == action[0]  and actioni[-1] == action[-1]: #la posicion inicial y la final son las mismas
                complete_action = actioni
                break


        if complete_action is not None:
            return self.excecute_action(complete_action)

        else:
            print("accion no permitida")
            return None

'''
class Board:
    def __init__(self,width,map):
        self._width = width
        self._map = map
    def width(self):
        return self._width
    def map(self):
        return self._map
'''
class SimpleChip:
    def __init__(self,width):
                        #UpRigthYPar, UpRigthYImpar
        self.directions = {"UR0": -width,"UR1": -(width-1),
                          #UpLeftYPar, UpLeftYImpar
                            "UL0": -(width+1),"UL1": -width,
                       #DownRigthYPar, DownRigthYImpar
                            "DR0":  width,"DR1":  width+1,
                        #DownLeftYPar, DowntLeftYImpar
                            "DL0":  width-1,"DL1":  width}
        self.xrestrictions = {"R0": False,"R1":width-1,
                              "L0": 0, "L1": False}

        self.yrestrictions = {"U":0 , "D":2*width-1}
        self.width = width
        self.dir_f = "{0}{1}{2}"
        self.rest_f = "{0}{1}"
    def all_movements(self,x,y,pos,directiony,board):
        simpleMovs, attackMoves = self.direct_movements(x,y,pos,directiony,board)
        if attackMoves:
            attackMoves = self.stringAttacks(attackMoves,board,directiony)
        return simpleMovs,attackMoves

    def direct_movements(self,x,y,pos,directiony,board):
        #print("x,y: ",x,",",y)
        #print("pos: ",pos)
        player,_ = board[pos]
        enemy = (not player)
        movements = []
        attackMovements = []
        par = y%2
        def try_movement(directionx):
            #movimiento hacia la directionx: izquierda, derecha
            restrictiony = self.yrestrictions[directiony]
            restrictionx = self.xrestrictions[self.rest_f.format(directionx,par)]
            if y != restrictiony and (restrictionx is False or x!= restrictionx):
                #hay tablero hacia la direccion y and x donde se va a mover
                displacement = self.directions[self.dir_f.format(directiony,directionx,par)]
                cell,_ = board[pos+displacement]
                if cell == enemy: #posible movimiento de ataque
                    restrictionx = self.xrestrictions[self.rest_f.format(directionx,int(not par))]
                    if y+math.copysign(1, displacement) != restrictiony and (restrictionx is False or x!= restrictionx):
                    #si hay una posicion final
                        displacement2 = self.directions[self.dir_f.format(directiony,directionx,int(not par))]
                        if board[pos+ displacement+displacement2][0] is None:#ahi no hay nada, se puede mover
                            #print("Attack Movement")
                            #                pose1      ,          pos2
                            movement = [pos+displacement, pos+displacement+displacement2]
                            # la ficha pasa de pos1 a pos dos matando la ficha que esta en pose1
                            attackMovements.append(movement)
                        #else: posicion ocupada
                    #else: esta en algun filo y no puede moverse mas hacia alla
                elif cell is None: # posicion vacia, se puede mover hacia alla
                    #print("Simple movement")
                    #           pos1,   pos2
                    movement = (pos+displacement)#posicion final
                    # la ficha pasa de pos1 a pos2 en un simple movimiento
                    movements.append(movement)
                #else: posicion ocupada no se puede mover
            #else: no puede moverse hacia arriba o hacia abajo porque se sale del tablero
        try_movement("R")#probar movimiento hacia la derecha
        try_movement("L")#probar movimiento hacia la izquierda
        return movements,attackMovements
    def stringAttacks(self,attacks,board,direction):
        for attack in  attacks:
            pos = attack[-1]
            x = pos % self.width
            y = int(pos /self.width)
            _, new_attacks = self.direct_movements(x,y,pos,direction,board)
            if not new_attacks:
                return attacks
            new_attacks = [(attack+new_attack) for new_attack in new_attacks]
            return self.stringAttacks(new_attacks,board,direction)

class QueenChip(SimpleChip):
    def direct_movements(self,pos,player,board):
        pass #TODO: retornar los posibles movimientos directos
