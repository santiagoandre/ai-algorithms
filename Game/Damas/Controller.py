from tkinter import *
from Damas import Damas
from ViewDamas import ViewDamas
class Controller:
    def __init__(self,jugarAbajo,width=8):
        if width%2!=0:#tiene que ser par
            width=8
        if not isinstance(jugarAbajo,bool):
            jugarAbajo  = True
        self.root = Tk()
        #crear modelo


        self.model=Damas()
        #crear vista
        self.view=ViewDamas(self.root,width)
        #unir componentes MVC
        self.view.canvas.bind("<Button>",self.__onClick)
        self.model.addView(self.view)



        #iniciar juego, aqui solo se crea el tablero, siempre empieza el de jugarAbajo
        self.model.play_game(True)

        #turnar a los jugadores(Maquina y usuario)
        self.turno = True # Siempre inicia el jugador de jugarAbajo
        self.jugador =jugarAbajo
        if not jugarAbajo:#empieza la maquina
            self.verificar(self.model.play_machine())
        self.posi = None
        self.root.mainloop()
    def verificar(self,respuestaModelo):
        if respuestaModelo is True:
            self.turno = not self.turno
        elif respuestaModelo is False:
            if self.model.utility(self.model.state,self.jugador)>0:
                print("you win")
            else:
                print("you lost")
            exit(0)

    def __onClick(self, event):
        if self.jugador !=self.turno:#no es su turno, esta penando la maquia
            print("No es tu turno")
            return
        i=int(event.y/self.view.cellheight)
        j=int(event.x/self.view.cellwidth)
        if j%2 ==  (i%2) :
            if i%2 != 0:
                j -=1
            j =  int(j/2)
            pos = int(self.view.grid_column*i/2+j)
            action = self.posi,pos
            if self.posi == pos:
                print("posi : ",self.posi)
                return
            if self.posi  is None:
                if  self.model.state["to_move"] == self.model.state["board"][pos][0]:
                    self.posi = pos
                    print("posi : ",self.posi)
            else:
                print("posf : ",pos)
                self.play_user(action)
                self.posi = None
    def play_user(self,action):
        respuestaModelo =self.model.play_user(action)
        self.verificar(respuestaModelo)
        if respuestaModelo is True:
            self.verificar(self.model.play_machine())
