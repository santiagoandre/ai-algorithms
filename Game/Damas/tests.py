from Damas import SimpleChip as SC
'''
0   1   2   3
   4   5   6   7
8   9   10  11
  12  13  14  15
16  17  18  19
  20  21  22  23
24  25  26  27
  28  29  30  31
'''
player1 = 1
player2 = -1
c = SC(4)
board  = [(0,c) for _ in range(32)]

board[2] = player1,c
board[5] = player2,c
print(c.direct_movements(0,0,2,'D',board))
