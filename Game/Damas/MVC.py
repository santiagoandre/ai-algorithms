

from abc import ABC, abstractmethod
class Model:
    def addView(self,view):
        self.views.append(view)
    def notify(self):
        for view in self.views:
            view.update(self.state)

        return []#TODO inicializar tablero
class View(ABC):
    @abstractmethod
    def update(self,model):
        pass
