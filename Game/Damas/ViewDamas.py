# -*- coding: utf-8 -*-

from tkinter import *
from MVC import View
class ViewDamas(View):

    def __init__(self, master,width):
        self.frame = Frame(master)
        self.frame.pack()
        self.height=600
        self.width=600
        self.grid_column=width
        self.grid_row=width
        self.canvas = Canvas(self.frame, height=self.height, width=self.width)
        self.cellwidth = int(self.canvas["width"])/self.grid_column
        self.cellheight = int(self.canvas["height"])/self.grid_row
        self.draw_grid()
        self.canvas.pack()
        self.player = 1
        self.pos=(0,0)



        self.hi_there = Button(self.frame, text="Jugar", command=self.start_Game)
        self.hi_there.pack(side=LEFT)


    def draw_grid(self):
        for i in range(self.grid_row):
            for j in range(self.grid_column):
                x1 = i * self.cellwidth
                y1 = j * self.cellheight
                x2 = x1 + self.cellwidth
                y2 = y1 + self.cellheight
                self.canvas.create_rectangle(x1, y1, x2, y2, fill="white")
    def update(self,model):
        board = model["board"]
        print("actualizando vista")
        logicpos = 0
        for logicpos in range(len(board)):
            player,_ = board[logicpos]
            self.drawChip(logicpos,player)
        self.frame.update()
    def drawChip(self):
        x=self.pos[1]*self.cellwidth
        y=self.pos[0]*self.cellheight
        if(self.player ==1):
            self.canvas.create_oval(x,y,x+self.cellwidth,y+self.cellheight, fill='blue')
            self.player=2
        else:
            self.canvas.create_oval(x,y,x+self.cellwidth,y+self.cellheight, fill='red')
            self.player=1


    def drawChips(self,board):
        for i in range(len(self.model)):
            row=boardl[i]
            for j in range(len(row)):
                val=board[i][j]
                x=j*self.cellwidth
                y=i*self.cellheight
                if(val ==1):
                    self.canvas.create_oval(x,y,x+self.cellwidth,y+self.cellheight, fill='blue')
                elif(val ==2):
                    self.canvas.create_oval(x,y,x+self.cellwidth,y+self.cellheight, fill='red')
    def sombrear_pos(self,pos):
        j = int(pos/self.grid_column*2)
        i =int(pos%(self.grid_column/2))
        i =  i*2
        if j%2 == 0:
            pass
        else:
            i +=1
        x=i*self.cellwidth
        y=j*self.cellheight
        #print("Crenado rectangulo pos: {0},{1}".format(pos,(i,j)) )
        self.canvas.create_rectangle(x,y,x+self.cellwidth,y+self.cellheight, fill='black')

    def drawChip(self,pos,to_move):
        j = int(pos/self.grid_column*2)
        i =int(pos%(self.grid_column/2))
        i =  i*2
        if j%2 == 0:
            pass
        else:
            i +=1
        x=i*self.cellwidth
        y=j*self.cellheight
        #print("Crenado rectangulo pos: {0},{1}".format(pos,(i,j)) )
        if to_move is None:
            color = "white"
        elif to_move:
            color = "red"
        else:
            color = "blue"
        self.canvas.create_oval(x,y,x+self.cellwidth,y+self.cellheight, fill=color)



    def  start_Game(self):
        print('start game')
